let $ = require('jquery');
window.$ = $;
window.jQuery = $;

// Vendor JS
require ('../../js/vendor/sliiide.js');

let angular = require('angular');
let Slideout = require('slideout');

let home = require('./pages/home');
let ticket = require('./partials/ticket');
let header = require('./partials/header');

header.init();