let $ = require ('jquery');

const ticket = {
	init : function () {
		let active = false;
		$(document).on('click','.btn-compare', function(){
			if(active)
			{
				$('#tickets').removeClass('active');
				$(this).html('COMPARE BENEFITS');
				active = false;
			}
			else
			{
				$('#tickets').addClass('active');
				$(this).html('CLOSE');
				active = true;
			}

			return false;
		});
	}
};

// call init here
ticket.init();

module.exports = ticket;