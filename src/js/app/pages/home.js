const home = {
    init : function () {
        let app = angular.module('home', []);
        app.controller('homeCtrl', function($scope) {
            $scope.intro = 'Fashion axe keytar truffaut migas Farm-to-table PBR&B. Drinking vinegar sustainable helvetica sartorial. Dreatmcatcher live-edge lo-fi, chartreuse echo park pinterest distillery glossier plaid fingerstache. Fashion axe keytar truffaut migas Farm-to-table PBR&B. Drinking vinegar sustainable helvetica';

            $scope.header = [
                {
                    'active': true,
                    'url': '#',
                    'template': 'partials/nav/single.html'
                },
                {
                    'active': false,
                    'url': '#',
                    'template': 'partials/nav/group.html'
                },
                {
                    'active': false,
                    'url': '#',
                    'template': 'partials/nav/investor.html'
                },
                {
                    'active': false,
                    'url': '#',
                    'template': 'partials/nav/startups.html'
                }
            ];

            $scope.investors = [
                'assets/img/workday1.png',
                'assets/img/workday2.png',
                'assets/img/workday3.png',
                'assets/img/workday4.png',
                'assets/img/workday5.png'
            ];

            $scope.tickets = [
                {
                    'popular': false,
                    'promocode': false,
                    'label':'SUMMER SAVER',
                    'price':'€1595',
                    'sublabel':'Save €300',
                    'date':'Until August 31.',
                    'url':'#',
                    'packages': [
                        'Cold-pressed poke thundercats brooklyn chillwave chartreuse',
                        'Craft beer 3 wolf moon tbh hoodie',
                        'YOLO synth hammock',
                        'Distillery aesthetic VHS affogato gentrify bespoke',
                        'Chia readymade schlitz brunch yuccie echo park',
                    ]
                },
                {
                    'popular': true,
                    'promocode': false,
                    'label':'EXPO ONLY',
                    'price':'€299',
                    'sublabel':'',
                    'date':'Until October',
                    'url':'#',
                    'packages': [
                        'Cold-pressed poke thundercats brooklyn chillwave chartreuse',
                        'Craft beer 3 wolf moon tbh hoodie',
                        'YOLO synth hammock',
                        'Distillery aesthetic VHS affogato gentrify bespoke',
                        'Chia readymade schlitz brunch yuccie echo park',
                    ]
                },
                {
                    'popular': false,
                    'promocode': false,
                    'label':'PUBLIC & GOVERNMENTAL SECTOR',
                    'price':'€1295',
                    'sublabel':'Save €300 fromt the Summer Saver',
                    'date':'Until October',
                    'url':'#',
                    'packages': [
                        'Cold-pressed poke thundercats brooklyn chillwave chartreuse',
                        'Craft beer 3 wolf moon tbh hoodie',
                        'YOLO synth hammock',
                        'Distillery aesthetic VHS affogato gentrify bespoke',
                        'Chia readymade schlitz brunch yuccie echo park',
                    ]
                },
                {
                    'popular': false,
                    'promocode': true,
                    'label':'STANDARD CONFERENCE & EXPO',
                    'price':'€1695',
                    'sublabel':'Do you have a promo code?',
                    'date':'',
                    'url':'#',
                    'packages': [
                        'Cold-pressed poke thundercats brooklyn chillwave chartreuse',
                        'Craft beer 3 wolf moon tbh hoodie',
                        'YOLO synth hammock',
                        'Distillery aesthetic VHS affogato gentrify bespoke',
                        'Chia readymade schlitz brunch yuccie echo park',
                    ]
                },
            ];

            $scope.footer = [
                {'label':'HRN','url':'#'},
                {'label':'ABOUT','url':'#'},
                {'label':'TEAM','url':'#'},
                {'label':'JOBS','url':'#'},
                {'label':'CONTACT','url':'#'},
            ];

            $scope.terms = [
                {'label':'HRN','url':'#'},
                {'label':'Terms & Conditions','url':'#'},
                {'label':'Cookie Policy','url':'#'},
                {'label':'Copyright © 2015 HRN Europe. All Rights Reserved.','url':''},
            ];

            $scope.social = [
                {'icon':'twitter','url':'#'},
                {'icon':'linkedin','url':'#'},
                {'icon':'facebook','url':'#'},
                {'icon':'instagram','url':'#'},
                {'icon':'slideshare','url':'#'},
                {'icon':'youtube-play','url':'#'},
            ];
        });
    }
};

// call init here
home.init();

module.exports = home;