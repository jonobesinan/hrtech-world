
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// Configuration - you can change these settings

// Destination - where all the files are save after processing. Defaults to /public/assets but you can change to
// something like public/wp-content/themes/THEME_NAME/assets for a Wordpress project

const webroot               = "../public/";
const themeFolderAssets     = webroot+"/assets/";
const spriteFolder          = '/sprite'; 

const vendorName            = "vendor";

const watchPaths    = [
    webroot+'**/*.php',
    themeFolderAssets+'css/*.css',
    themeFolderAssets+'js/*.js',
];

// Plugins
const path                  = require('path');
const webpack               = require('webpack');
const WatchLiveReloadPlugin = require('webpack-watch-livereload-plugin');
const LiveReloadPlugin      = require('webpack-livereload-plugin');
const ExtractTextPlugin     = require('extract-text-webpack-plugin');
const WebpackNotifierPlugin = require('webpack-notifier');
const SpritesmithPlugin     = require('webpack-spritesmith');
const UglifyJSPlugin        = require('uglifyjs-webpack-plugin');

module.exports = {
    entry: {
        app: [
            './js/app/app.js',
            './scss/app.scss'
        ],
    },
    output: {
        path: path.resolve(__dirname, themeFolderAssets),
        // publicPath: 'http://localhost:8080/assets/',
        // publicPath: '/assets/',
        filename: 'js/[name].js',
    },
    plugins: [
        new UglifyJSPlugin({
            mangle: false,
            output: {
                beautify: true,
                ascii_only: true
            }
        }),
        new WebpackNotifierPlugin({
            alwaysNotify: true,
        }),
        new webpack.NamedModulesPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            name: vendorName,
            filename: 'js/[name].js',
            minChunks(module, count) {
                let context = module.context;
                return context && context.indexOf('node_modules') >= 0;
            },
        }),
        new webpack.ProvidePlugin({
            jQuery: 'jquery',
            $: 'jquery',
            "window.jQuery": "jquery",
            "window.$": "jquery",
        }),
        new ExtractTextPlugin("css/[name].css"),
        new webpack.SourceMapDevToolPlugin({
            filename: 'js/[name].js.map',
            // exclude: ['vendor.js']
        }),
        // new LiveReloadPlugin(liveReploadOptions),
        new WatchLiveReloadPlugin({
            files: watchPaths
        }),
        new SpritesmithPlugin({
            src: {
                cwd: path.resolve( __dirname, 'sprite' ),
                glob: '*.png'
            },
            target: {
                image: path.resolve( themeFolderAssets, 'img/sprite.png'),
                css: path.resolve( __dirname, 'scss/_sprite.scss')
            },
            apiOptions: {
                cssImageRef: "../img/sprite.png"
            }
        })
    ],
    module: {
        rules: [
            {
                test: /.js$/,
                exclude: [/node_modules/],
                use: [{
                    loader: 'buble-loader',
                }],
            },
            {
                test: /\.(sass|scss)$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader?minimize=true&url=false!sass-loader",
                }),
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader"
                })
            },
            {
                test: /\.(png|jpg)$/,
                loader: 'url-loader?limit=8192'
            }, // inline base64 URLs for <=8k images, direct URLs for the rest
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                // loader: 'url-loader'
                loader: 'file-loader?publicPath=../&name=fonts/[name].[ext]'
            }
        ]
    },
    devServer: {
        contentBase: path.resolve(__dirname, webroot),
        // host: '0.0.0.0',
        // port: 9000,
        disableHostCheck: true,
        stats: {
            colors: true
        },
        headers: {
            "Access-Control-Allow-Origin": "*",
        }
    },
};