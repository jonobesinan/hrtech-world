webpackJsonp([ 0 ], {
    "./js/app/app.js": function(module, exports, __webpack_require__) {
        (function(__webpack_provided_window_dot_$, __webpack_provided_window_dot_jQuery) {
            var $ = __webpack_require__("./node_modules/jquery/dist/jquery.js");
            __webpack_require__("./js/vendor/sliiide.js");
            __webpack_require__("./node_modules/angular/index.js"), __webpack_require__("./node_modules/slideout/index.js"), 
            __webpack_require__("./js/app/pages/home.js"), __webpack_require__("./js/app/partials/ticket.js");
            __webpack_require__("./js/app/partials/header.js").init();
        }).call(exports, __webpack_require__("./node_modules/jquery/dist/jquery.js"), __webpack_require__("./node_modules/jquery/dist/jquery.js"));
    },
    "./js/app/pages/home.js": function(module, exports) {
        var home = {
            init: function() {
                angular.module("home", []).controller("homeCtrl", function($scope) {
                    $scope.intro = "Fashion axe keytar truffaut migas Farm-to-table PBR&B. Drinking vinegar sustainable helvetica sartorial. Dreatmcatcher live-edge lo-fi, chartreuse echo park pinterest distillery glossier plaid fingerstache. Fashion axe keytar truffaut migas Farm-to-table PBR&B. Drinking vinegar sustainable helvetica", 
                    $scope.header = [ {
                        active: !0,
                        url: "#",
                        template: "partials/nav/single.html"
                    }, {
                        active: !1,
                        url: "#",
                        template: "partials/nav/group.html"
                    }, {
                        active: !1,
                        url: "#",
                        template: "partials/nav/investor.html"
                    }, {
                        active: !1,
                        url: "#",
                        template: "partials/nav/startups.html"
                    } ], $scope.investors = [ "assets/img/workday1.png", "assets/img/workday2.png", "assets/img/workday3.png", "assets/img/workday4.png", "assets/img/workday5.png" ], 
                    $scope.tickets = [ {
                        popular: !1,
                        promocode: !1,
                        label: "SUMMER SAVER",
                        price: "\u20ac1595",
                        sublabel: "Save \u20ac300",
                        date: "Until August 31.",
                        url: "#",
                        packages: [ "Cold-pressed poke thundercats brooklyn chillwave chartreuse", "Craft beer 3 wolf moon tbh hoodie", "YOLO synth hammock", "Distillery aesthetic VHS affogato gentrify bespoke", "Chia readymade schlitz brunch yuccie echo park" ]
                    }, {
                        popular: !0,
                        promocode: !1,
                        label: "EXPO ONLY",
                        price: "\u20ac299",
                        sublabel: "",
                        date: "Until October",
                        url: "#",
                        packages: [ "Cold-pressed poke thundercats brooklyn chillwave chartreuse", "Craft beer 3 wolf moon tbh hoodie", "YOLO synth hammock", "Distillery aesthetic VHS affogato gentrify bespoke", "Chia readymade schlitz brunch yuccie echo park" ]
                    }, {
                        popular: !1,
                        promocode: !1,
                        label: "PUBLIC & GOVERNMENTAL SECTOR",
                        price: "\u20ac1295",
                        sublabel: "Save \u20ac300 fromt the Summer Saver",
                        date: "Until October",
                        url: "#",
                        packages: [ "Cold-pressed poke thundercats brooklyn chillwave chartreuse", "Craft beer 3 wolf moon tbh hoodie", "YOLO synth hammock", "Distillery aesthetic VHS affogato gentrify bespoke", "Chia readymade schlitz brunch yuccie echo park" ]
                    }, {
                        popular: !1,
                        promocode: !0,
                        label: "STANDARD CONFERENCE & EXPO",
                        price: "\u20ac1695",
                        sublabel: "Do you have a promo code?",
                        date: "",
                        url: "#",
                        packages: [ "Cold-pressed poke thundercats brooklyn chillwave chartreuse", "Craft beer 3 wolf moon tbh hoodie", "YOLO synth hammock", "Distillery aesthetic VHS affogato gentrify bespoke", "Chia readymade schlitz brunch yuccie echo park" ]
                    } ], $scope.footer = [ {
                        label: "HRN",
                        url: "#"
                    }, {
                        label: "ABOUT",
                        url: "#"
                    }, {
                        label: "TEAM",
                        url: "#"
                    }, {
                        label: "JOBS",
                        url: "#"
                    }, {
                        label: "CONTACT",
                        url: "#"
                    } ], $scope.terms = [ {
                        label: "HRN",
                        url: "#"
                    }, {
                        label: "Terms & Conditions",
                        url: "#"
                    }, {
                        label: "Cookie Policy",
                        url: "#"
                    }, {
                        label: "Copyright \xa9 2015 HRN Europe. All Rights Reserved.",
                        url: ""
                    } ], $scope.social = [ {
                        icon: "twitter",
                        url: "#"
                    }, {
                        icon: "linkedin",
                        url: "#"
                    }, {
                        icon: "facebook",
                        url: "#"
                    }, {
                        icon: "instagram",
                        url: "#"
                    }, {
                        icon: "slideshare",
                        url: "#"
                    }, {
                        icon: "youtube-play",
                        url: "#"
                    } ];
                });
            }
        };
        home.init(), module.exports = home;
    },
    "./js/app/partials/header.js": function(module, exports, __webpack_require__) {
        var $ = __webpack_require__("./node_modules/jquery/dist/jquery.js"), header = (__webpack_require__("./js/vendor/sliiide.js"), 
        {
            init: function() {
                var settings = {
                    toggle: ".btn-toggle-menu",
                    animation_duration: "0.5s",
                    place: "right",
                    animation_curve: "cubic-bezier(0.54, 0.01, 0.57, 1.03)",
                    body_slide: !0,
                    no_scroll: !1,
                    auto_close: !1
                };
                $("#menu").sliiide(settings);
            }
        });
        module.exports = header;
    },
    "./js/app/partials/ticket.js": function(module, exports, __webpack_require__) {
        var $ = __webpack_require__("./node_modules/jquery/dist/jquery.js"), ticket = {
            init: function() {
                var active = !1;
                $(document).on("click", ".btn-compare", function() {
                    return active ? ($("#tickets").removeClass("active"), $(this).html("COMPARE BENEFITS"), 
                    active = !1) : ($("#tickets").addClass("active"), $(this).html("CLOSE"), active = !0), 
                    !1;
                });
            }
        };
        ticket.init(), module.exports = ticket;
    },
    "./js/vendor/sliiide.js": function(module, exports, __webpack_require__) {
        (function(jQuery) {
            !function($) {
                function preventDefault(e) {
                    e = e || window.event, e.preventDefault && e.preventDefault(), e.returnValue = !1;
                }
                function preventDefaultForScrollKeys(e) {
                    if (keys[e.keyCode]) return preventDefault(e), !1;
                }
                function disable_scroll() {
                    window.addEventListener && window.addEventListener("DOMMouseScroll", preventDefault, !1), 
                    window.onwheel = preventDefault, window.onmousewheel = document.onmousewheel = preventDefault, 
                    window.ontouchmove = preventDefault, document.onkeydown = preventDefaultForScrollKeys;
                }
                function enable_scroll() {
                    window.removeEventListener && window.removeEventListener("DOMMouseScroll", preventDefault, !1), 
                    window.onmousewheel = document.onmousewheel = null, window.onwheel = null, window.ontouchmove = null, 
                    document.onkeydown = null;
                }
                var ie = function() {
                    var ua = window.navigator.userAgent, msie = ua.indexOf("MSIE ");
                    if (msie > 0) return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
                    if (ua.indexOf("Trident/") > 0) {
                        var rv = ua.indexOf("rv:");
                        return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
                    }
                    var edge = ua.indexOf("Edge/");
                    return edge > 0 && parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
                }();
                $.fn.sliiide = function(options) {
                    function deactivate() {
                        $body.one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", hideSlider), 
                        settings.body_slide ? ($body.children().css(prefixCSS(bodySlideProp[settings.place].deactivateAnimation)), 
                        !1 !== ie && ie <= 11 && $sliiider.css(prefixCSS(Prop[settings.place].deactivateAnimation))) : $sliiider.css(prefixCSS(Prop[settings.place].deactivateAnimation)), 
                        settings.no_scroll && enable_scroll(), clicked = !1;
                    }
                    var newSize, bodySlideDistance, settings = $.extend({
                        toggle: "#sliiider-toggle",
                        exit_selector: ".slider-exit",
                        animation_duration: "0.5s",
                        place: "right",
                        animation_curve: "cubic-bezier(0.54, 0.01, 0.57, 1.03)",
                        body_slide: !0,
                        no_scroll: !1,
                        auto_close: !1
                    }, options), clicked = !1, $sliiider = $(this), $toggle = $(settings.toggle), $exit = $(settings.exit_selector), $body = $("body"), bodyResetProp = {
                        transform: "",
                        "overflow-x": "",
                        transition: "",
                        position: ""
                    }, sliiiderResetProp = {
                        transform: "",
                        transition: "",
                        width: "",
                        height: "",
                        left: "",
                        top: "",
                        bottom: "",
                        right: ""
                    }, prepareProperties = {
                        visibility: "hidden",
                        transition: "transform " + settings.animation_duration + " " + settings.animation_curve,
                        position: "fixed"
                    }, bodyChildrenProp = {
                        transition: "transform " + settings.animation_duration + " " + settings.animation_curve
                    }, htmlProp = {
                        "overflow-x": "hidden"
                    }, bodySlidePrepare = {
                        position: "relative",
                        "overflow-x": "hidden"
                    }, bodySlideProp = {
                        setleft: function(distance) {
                            this.left.activateAnimation.transform = "translateX(" + distance + "px)", this.left.deactivateAnimation.transform = "translateX(0px)";
                        },
                        setright: function(distance) {
                            this.right.activateAnimation.transform = "translateX(-" + distance + "px)", this.right.deactivateAnimation.transform = "translateX(0px)";
                        },
                        setbottom: function(distance) {
                            this.bottom.activateAnimation.transform = "translateY(-" + distance + "px)", this.bottom.deactivateAnimation.transform = "translateY(0px)";
                        },
                        settop: function(distance) {
                            this.top.activateAnimation.transform = "translateY(" + distance + "px)", this.top.deactivateAnimation.transform = "translateY(0px)";
                        },
                        left: {
                            activateAnimation: {
                                transform: ""
                            },
                            deactivateAnimation: {
                                transform: ""
                            }
                        },
                        right: {
                            activateAnimation: {
                                transform: ""
                            },
                            deactivateAnimation: {
                                transform: ""
                            }
                        },
                        top: {
                            activateAnimation: {
                                transform: ""
                            },
                            deactivateAnimation: {
                                transform: ""
                            }
                        },
                        bottom: {
                            activateAnimation: {
                                transform: ""
                            },
                            deactivateAnimation: {
                                transform: ""
                            }
                        }
                    }, Prop = {
                        left: {
                            properties: function() {
                                return {
                                    top: "0",
                                    left: "-" + $sliiider.width() + "px"
                                };
                            },
                            activateAnimation: {
                                transform: "translateX(100%)"
                            },
                            deactivateAnimation: {
                                transform: "translateX(0)"
                            },
                            size: function(wHeight, wWidth) {
                                return {
                                    height: wHeight
                                };
                            }
                        },
                        right: {
                            properties: function() {
                                return {
                                    top: "0",
                                    right: "-" + $sliiider.width() + "px"
                                };
                            },
                            activateAnimation: {
                                transform: "translateX(-100%)"
                            },
                            deactivateAnimation: {
                                transform: "translateX(0)"
                            },
                            size: function(wHeight, wWidth) {
                                return {
                                    height: wHeight
                                };
                            }
                        },
                        top: {
                            properties: function() {
                                return {
                                    left: "0",
                                    right: "0",
                                    top: "-" + $sliiider.height() + "px"
                                };
                            },
                            activateAnimation: {
                                transform: "translateY(100%)"
                            },
                            deactivateAnimation: {
                                transform: "translateY(0)"
                            },
                            size: function(wHeight, wWidth) {
                                return {
                                    width: wWidth
                                };
                            }
                        },
                        bottom: {
                            properties: function() {
                                return {
                                    left: 0,
                                    right: 0,
                                    bottom: "-" + $sliiider.height() + "px"
                                };
                            },
                            activateAnimation: {
                                transform: "translateY(-100%)"
                            },
                            deactivateAnimation: {
                                transform: "translateY(0)"
                            },
                            size: function(wHeight, wWidth) {
                                return {
                                    width: wWidth
                                };
                            }
                        }
                    }, prefixCSS = function(cssProp) {
                        return $.each(cssProp, function(k, v) {
                            if ("transition" === k) {
                                var trnsCSS = {}, trnsProp = v.split(" ", 1)[0], trnsAttr = v.split(" ");
                                trnsAttr.shift(), trnsAttr = trnsAttr.join(" "), trnsCSS["-webkit-" + k] = "-webkit-" + trnsProp + " " + trnsAttr, 
                                trnsCSS["-ms-" + k] = "-ms-" + trnsProp + " " + trnsAttr, $.extend(cssProp, trnsCSS);
                            } else if ("transform" === k) {
                                var trnsfCSS = {};
                                trnsfCSS["-webkit-" + k] = v, trnsfCSS["-ms-" + k] = v;
                            }
                        }), cssProp;
                    }, siiize = function() {
                        var windowSize = {}, scroll = getScrollBarWidth();
                        windowSize.height = $(window).height(), windowSize.width = $(window).width() + scroll, 
                        newSize = Prop[settings.place].size(windowSize.height, windowSize.width), $sliiider.css(newSize), 
                        $sliiider.css(prefixCSS(Prop[settings.place].properties())), setSlideDistance();
                    }, setSlideDistance = function() {
                        settings.body_slide && (bodySlideDistance = "right" === settings.place || "left" === settings.place ? $sliiider.width() : $sliiider.height(), 
                        bodySlideProp["set" + settings.place](bodySlideDistance));
                    }, prepare = function() {
                        $sliiider.css(prefixCSS(prepareProperties)), $sliiider.css(prefixCSS(Prop[settings.place].properties())), 
                        setSlideDistance();
                    }, getScrollBarWidth = function() {
                        var inner = document.createElement("p");
                        inner.style.width = "100%", inner.style.height = "200px";
                        var outer = document.createElement("div");
                        outer.style.position = "absolute", outer.style.top = "0px", outer.style.left = "0px", 
                        outer.style.visibility = "hidden", outer.style.width = "200px", outer.style.height = "150px", 
                        outer.style.overflow = "hidden", outer.appendChild(inner), document.body.appendChild(outer);
                        var w1 = inner.offsetWidth;
                        outer.style.overflow = "scroll";
                        var w2 = inner.offsetWidth;
                        return w1 === w2 && (w2 = outer.clientWidth), document.body.removeChild(outer), 
                        w1 - w2;
                    }, activate = function() {
                        if (siiize(), $sliiider.css("visibility", "visible"), settings.body_slide) {
                            $body.css(prefixCSS(bodySlidePrepare)), $("html").css(htmlProp), $body.children().css(prefixCSS(bodyChildrenProp)), 
                            $body.children().css(prefixCSS(bodySlideProp[settings.place].activateAnimation)), 
                            !1 !== ie && ie <= 11 && $sliiider.css(prefixCSS(Prop[settings.place].activateAnimation));
                            $(window).height(), $(window).scrollTop(), $sliiider.height(), $sliiider.offset().top;
                        } else $sliiider.css(prefixCSS(Prop[settings.place].activateAnimation));
                        settings.no_scroll && disable_scroll(), clicked = !0;
                    }, hideSlider = function(e) {
                        $sliiider.css("visibility", "hidden"), $body.css(bodyResetProp), $("html").css(bodyResetProp), 
                        $body.unbind("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", hideSlider), 
                        prepare();
                    };
                    siiize(), prepare(), $(window).resize(siiize), $sliiider.resize(siiize);
                    var handleToggle = function() {
                        clicked ? deactivate() : activate();
                    };
                    $toggle.click(handleToggle), settings.auto_close && $sliiider.find("a").on("click", function() {
                        deactivate();
                    }), $exit.on("click", function() {
                        deactivate();
                    });
                    var deleteProp = function() {
                        $body.css(bodyResetProp), $sliiider.css(sliiiderResetProp), $(window).off("resize", siiize), 
                        $toggle.off("click", handleToggle);
                    };
                    return {
                        reset: function(name) {
                            $body.one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", deleteProp), 
                            deactivate();
                        },
                        deactivate: function() {
                            deactivate();
                        },
                        activate: function() {
                            activate();
                        }
                    };
                };
                var keys = {
                    37: 1,
                    38: 1,
                    39: 1,
                    40: 1
                };
            }(jQuery);
        }).call(exports, __webpack_require__("./node_modules/jquery/dist/jquery.js"));
    },
    "./scss/app.scss": function(module, exports) {},
    0: function(module, exports, __webpack_require__) {
        __webpack_require__("./js/app/app.js"), module.exports = __webpack_require__("./scss/app.scss");
    }
}, [ 0 ]);